import './App.css';
import Menu from './Sections/Menu.js'
import AboutMe from './Sections/AboutMe.js'
import Objectives from './Sections/Objectives.js'
import TranSkills from './Sections/TranSkills.js'
import Experiences from './Sections/Experiences.js';
import Work from './Sections/Work.js';
import Education from './Sections/Education.js';
import Languages from './Sections/Languages.js';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {Menu()}
      </header>
      {AboutMe()}
      {Objectives()}
      {TranSkills()}
      {Experiences()}
      {Work()}
      {Education()}
      {Languages()}
    </div>
  );
}

export default App;
