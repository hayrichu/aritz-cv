
function Menu() {
  return (
        <ul className="Menu">
        <li><a href="#App">Home</a></li>
        <li><a href="AboutMe">About me</a></li>
        <li><a href="#Objectives">Professional Objectives</a></li>
        <li><a href="#TranSkills">Transversal Skills</a></li>
        <li><a href="#Experiences">Significant Experiences</a></li>
        <li><a href="#Work">Work Experience</a></li>
        <li><a href="#Education">Education</a></li>
        <li><a href="#Languages">Languages</a></li>
        </ul>
  );
}


export default Menu;