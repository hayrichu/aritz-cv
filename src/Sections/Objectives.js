function Objectives(){
    return(
<div id="Objectives">
<h1>Profesional Objectives</h1>   
<p>
I would like to work in a company where I can learn from and with my colleagues, where creating the best possible product or service and improving people's lives is the highest priority.
</p>
<p>
I want to work with a group of people with whom to collaborate, grow and learn in which the values ​​of companionship, integrity, and respect are the pillars of its values. 
</p>
<p>
    I am currently studying to become a Cloud Engineer. So I am especially interested in all cloud-oriented environments that are considering a transition to it as part of their services in the near future. I am currently studying to become an AWS Solutions Architect Associate.
</p>
</div>        
);
}
export default Objectives;