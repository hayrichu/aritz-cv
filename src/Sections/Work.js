import React, { useState, useEffect } from 'react';
import GetSection from './GetSection.js'

function Work(){
        const[works,setWorks] = useState([])
        useEffect(() => {
             GetSection("Work").then(items => { setWorks({items})})
          
          }, [])
          if(works.length !== 0 ){
                return(
                    <div id="Work">
                        <h1>Work Experiences</h1>
                        {
                            
                            works.items.map((data,key) => {
                                return (
                                    <div>
                                    <h2 className="WorkTitle"> {data.Work} ({data.DurationDescription}) / {data.Position}</h2>
                                    <p className="WorkDescription">{data.GeneralDescription}</p>
                                    {PopulateProjects(data.Projects)}
                                    {PopulateReferences(data.References)}
                                    </div>
                                )
                            })
                        }
                        
                    </div>
                );
         }
}

function PopulateProjects(projects){
    if(!projects !== true){
    return(
       <ul>
            {
                projects.map((data,key) => {
                    return(
                    <div>
                    <p>{data.Project}</p>
                    <ul>
                     {
                         PopulateActivities(data.Activities)
                     }
                        <ul>
                            <li>{data.Tech}</li>
                         </ul>
                    </ul>            
                  
                    </div>    
                    )
                })
            }
        </ul>
    )
    }
}

function PopulateActivities(activities){
    if(!activities !== true){
        return(
            <div>
            {
                activities.map((activity,key) =>{
                    return(
                        <li>{activity.Activity}</li>
                    )
                })
            }
            </div>   
        )
    }
}

function PopulateReferences(references){
    if(!references !== true){
        return(
            <div>
            <h3>References</h3>
                <ul>
                {
                    references.map((data,key) => {
                        return (
                            <li>
                            <a className="referenceName" href={data.Linkedin}>{data.Name}</a>
                            <p className="referenceDescription">{data.Description}</p>
                            </li>
                        )
                    })
                }
                </ul>
            </div>
        )
    }        
}

export default Work