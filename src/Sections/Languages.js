function Languages(){
    return(
    <div id="Languages">
    <h1>Languages</h1>
    <ul>
        <li>Spanish, spoken and written (Native language)</li>
        <li>Catalan, spoken and written (Professional level)</li>
        <li>English, spoken and written (Professional level)</li>
    </ul>
    </div>
    );
}

export default Languages