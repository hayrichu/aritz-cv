import React, { useState, useEffect } from 'react';
import GetSection from './GetSection.js'
function AboutMe(){
    const[paragraphs,setParagraphs] = useState([])
    useEffect(() => {
         GetSection("AboutMe").then(items => { setParagraphs({items})})
      }, [])
    if(paragraphs.length !== 0){
            return(
            <div id="AboutMe">
            <h1>About Me</h1>   
                    {
                        paragraphs.items.map((data,key) => {
                            console.log(data)
                            return (
                                <p>{data.Paragraph}</p>
                            )
                        })
                    }
            </div>

            );
    }
}
export default AboutMe;
