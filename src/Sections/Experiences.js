function Experiences(){
    return(
    <div  id="Experiences">
    <h1>Significant Experiences</h1>
    <ul>
        <li>Leading local IT management in an international project in which 2 marketing campaign platforms were migrated and merged from two companies to a new common system in Adobe Campaign.</li>
        <li>Participate in the integration of corporate retail applications in the merger of the two largest audiology companies in Spain. Using services WCF in .Net Framework and APIs REST in.NetCore depending on the use case.</li>
        <li>Working remotely for more than 7 years in a company not designed for it and that my voice was still being heard and being important throughout that time.</li>
        <li>Participate in the transition of applications from VB6 and WinForms to WPF using pattern, MVVC helping to create what would later be used as the basis for creating the rest of the new applications.</li>
        <li>Stabilize, maintain, grow and bring to fruition, tools business intelligence on OLAP databases from an initial external project.</li>
        <li>Migrate and transform during my first year as a programmer a management and communication portal with employees from an existing one in ASP (vb6) created with a code generator (Genexus) to a modern application for the time in .Net (Webforms).</li>
    </ul>
    </div>
    );
}
export default Experiences