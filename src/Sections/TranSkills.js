function TranSkills(){
return(
<div id="TranSkills">
<h1>Transversal Skills</h1>   
<ul>
    <li>I am decisive and capable of overcoming the adversities that I face.</li>
    <li>I create strong relationships with peers based on trust.</li>
    <li>Ability to adapt to the environment and learn on the go.</li>
</ul>
</div>
);
}
export default TranSkills;